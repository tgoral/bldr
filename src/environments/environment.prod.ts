export const environment = {
  production: true,
  baseUrl: 'https://marblejs-example.herokuapp.com/api/v1'
};
