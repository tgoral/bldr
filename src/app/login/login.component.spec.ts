import { BehaviorSubject } from 'rxjs';
import { LoginComponent } from './login.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthFacade } from '../core/services/auth.facade';
import { LayoutFacade } from '../core/services/layout.facade';
import { LoginModule } from './login.module';
describe('LoginComponent', () => {
  let fixture: ComponentFixture<LoginComponent>;
  let authFacade: jasmine.SpyObj<AuthFacade>;
  let loading$: BehaviorSubject<boolean>;
  let errorMessage$: BehaviorSubject<string>;
  let page: PageObject;

  beforeEach(() => {
    loading$ = new BehaviorSubject(false);
    errorMessage$ = new BehaviorSubject(null);
    authFacade = jasmine.createSpyObj('authFacade', ['login']);
    authFacade.errorMessage$ = errorMessage$.asObservable();
    TestBed.configureTestingModule({
      imports: [LoginModule],
      providers: [
        LayoutFacade,
        { provide: AuthFacade, useValue: authFacade },
        { provide: LayoutFacade, useValue: { loading$ } }
      ]
    });
    fixture = TestBed.createComponent(LoginComponent);
    page = new PageObject(fixture);
    fixture.detectChanges();
  });

  it('should show spinner on login button when loading', () => {
    expect(page.isSpinnerVisible).toBeFalsy();
    loading$.next(true);
    fixture.detectChanges();
    expect(page.isSpinnerVisible).toBeTruthy();
  });

  it('should show error message', () => {
    expect(page.errorMessage).toBeNull();
    const error = 'teeeest error';
    errorMessage$.next(error);
    fixture.detectChanges();
    expect(page.errorMessage.innerText.trim()).toEqual(error);
    errorMessage$.next(null);
    fixture.detectChanges();
    expect(page.errorMessage).toBeNull();
  });

  it('should call AuthFacade on submit login', () => {
    expect(page.submitButton.disabled).toBeTruthy();
    const login = 'email@email.com';
    const password = '123456';
    page.setLogin(login);
    expect(page.submitButton.disabled).toBeTruthy();
    page.setPassword(password);
    expect(page.submitButton.disabled).toBeFalsy();
    page.submitButton.click();
    expect(authFacade.login).toHaveBeenCalledWith({ login, password });
  });
});

class PageObject {
  constructor(private fixture: ComponentFixture<LoginComponent>) {}

  get isSpinnerVisible(): boolean {
    return this.fixture.nativeElement.querySelectorAll('app-spinner').length > 0;
  }

  get errorMessage(): HTMLElement {
    return this.fixture.nativeElement.querySelector('.error-msg');
  }

  get submitButton(): HTMLButtonElement {
    return this.fixture.nativeElement.querySelector('button');
  }

  get login(): HTMLInputElement {
    return this.fixture.nativeElement.querySelector('#login');
  }

  get password(): HTMLInputElement {
    return this.fixture.nativeElement.querySelector('#password');
  }

  setLogin(value: string) {
    this.login.value = value;
    this.login.dispatchEvent(new Event('input'));
    this.fixture.detectChanges();
  }

  setPassword(value: string) {
    this.password.value = value;
    this.password.dispatchEvent(new Event('input'));
    this.fixture.detectChanges();
  }
}
