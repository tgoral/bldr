import { LayoutFacade } from '../core/services/layout.facade';
import { AuthFacade } from '../core/services/auth.facade';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  login = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', Validators.minLength(4));
  loading$ = this.layoutFacade.loading$;
  errorMessage: string;
  private ngUnsubscribe = new Subject();

  constructor(private authFacade: AuthFacade, private layoutFacade: LayoutFacade) {}

  ngOnInit() {
    this.authFacade.errorMessage$.subscribe(err => (this.errorMessage = err));
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  submit() {
    this.authFacade.login({ login: this.login.value, password: this.password.value });
  }
}
