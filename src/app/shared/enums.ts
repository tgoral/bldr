export function isFromEnum<E, K extends string | number>(
  e: { [key in K]: E },
  candidate: string | number
): boolean {
  return !!Object.keys(e).find(enumKey => e[enumKey] === candidate);
}

export enum AppIcons {
  LOGOUT = '_ionicons_svg_md-log-out',
  FILM = '_ionicons_svg_md-film',
  ARROW_UP = '_ionicons_svg_md-arrow-up',
  LEFT_ARROW = '_ionicons_svg_md-arrow-dropleft',
  RIGHT_ARROW = '_ionicons_svg_md-arrow-dropright'
}

export enum MoviesSortBy {
  Title = 'title',
  Year = 'year',
  Metacode = 'metacode'
}

export enum SortDir {
  Asc = -1,
  Desc = 1
}
