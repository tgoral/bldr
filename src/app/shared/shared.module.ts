import { SpinnerModule } from './spinner/spinner.module';
import { DataTableModule } from './data-table/data-table.module';
import { PaginationModule } from './pagination/pagination.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { IconModule } from './icon/icon.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PaginationModule,
    IconModule,
    DataTableModule,
    SpinnerModule
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PaginationModule,
    IconModule,
    DataTableModule,
    SpinnerModule
  ]
})
export class SharedModule {}
