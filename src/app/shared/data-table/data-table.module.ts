import { SpinnerModule } from './../spinner/spinner.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableComponent } from './data-table.component';
import { IconModule } from '../icon/icon.module';

@NgModule({
  declarations: [DataTableComponent],
  imports: [CommonModule, IconModule, SpinnerModule],
  exports: [DataTableComponent]
})
export class DataTableModule {}
