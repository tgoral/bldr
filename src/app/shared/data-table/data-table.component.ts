import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { AppIcons, SortDir } from './../enums';
import { Sort, TableColumn } from './../interfaces';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DataTableComponent {
  @Input() rows: object[];
  @Input() datatableIdxOffset: number;
  @Input() columns: TableColumn[] = [];
  @Input() sort: Sort;
  @Input() spinner: boolean;
  @Output() select = new EventEmitter<object>();
  @Output() columnHeaderClick = new EventEmitter<{ columnKey: string }>();

  icons = AppIcons;
  sortDir = SortDir;

  constructor() {}

  onSelect(row: object) {
    this.select.emit(row);
  }

  onColumnHeaderClick(columnKey: string) {
    this.columnHeaderClick.emit({ columnKey });
  }
}
