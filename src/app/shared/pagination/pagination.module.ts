import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationComponent } from './pagination.component';
import { IconModule } from '../icon/icon.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [PaginationComponent],
  imports: [CommonModule, IconModule, ReactiveFormsModule],
  exports: [PaginationComponent]
})
export class PaginationModule {}
