import { FormControl } from '@angular/forms';
import { AppIcons } from './../enums';
import {
  Component,
  Input,
  OnChanges,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy
} from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginationComponent implements OnChanges, OnInit, OnDestroy {
  @Input() itemsPerPage: number;
  @Input() totalNumber: number;
  @Input() page: number;
  @Output() pageChange = new EventEmitter<number>();
  @Output() itemsPerPageChange = new EventEmitter<number>();

  itemsPerPageControl = new FormControl();
  numberOfPages: number;
  icons = AppIcons;
  private ngUnsubscribe$ = new Subject();

  constructor() {}

  ngOnInit() {
    this.itemsPerPageControl.valueChanges
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        filter(value => value > 0),
        takeUntil(this.ngUnsubscribe$)
      )
      .subscribe(value => {
        this.itemsPerPageChange.emit(value);
      });
  }

  ngOnChanges() {
    this.loadPagination();
    if (this.itemsPerPage !== this.itemsPerPageControl.value) {
      this.itemsPerPageControl.patchValue(this.itemsPerPage);
    }
  }

  ngOnDestroy() {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  previousPage() {
    this.pageChange.emit(this.page - 1);
  }

  nextPage() {
    this.pageChange.emit(this.page + 1);
  }

  loadPagination() {
    this.numberOfPages = Math.ceil(this.totalNumber / this.itemsPerPage);
  }
}
