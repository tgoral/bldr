import { Component, Output, EventEmitter } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { PaginationModule } from './pagination.module';
describe('PaginationComponent', () => {
  let fixture: ComponentFixture<TestHostComponent>;
  let page: PageObject;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestHostComponent],
      imports: [PaginationModule]
    });
    fixture = TestBed.createComponent(TestHostComponent);
    page = new PageObject(fixture);
    fixture.detectChanges();
  });

  it('should display switch buttons only when items per page are less than total number', () => {
    fixture.componentInstance.itemsPerPage = 10;
    fixture.componentInstance.totalNumber = 5;
    fixture.componentInstance.page = 1;
    fixture.detectChanges();
    expect(page.areSwitchIconsVisible).toBeFalsy();
    fixture.componentInstance.itemsPerPage = 3;
    fixture.detectChanges();
    expect(page.areSwitchIconsVisible).toBeTruthy();
  });

  it('should disable switch button on first and last page', () => {
    fixture.componentInstance.itemsPerPage = 2;
    fixture.componentInstance.totalNumber = 4;
    fixture.componentInstance.page = 1;
    fixture.detectChanges();
    expect(page.previousPageButton.disabled).toBeTruthy();
    expect(page.nextPageButton.disabled).toBeFalsy();
    fixture.componentInstance.page = 2;
    fixture.detectChanges();
    expect(page.nextPageButton.disabled).toBeTruthy();
    expect(page.previousPageButton.disabled).toBeFalsy();
  });

  it('should emit itemsPerPageChange event with distinctUntilChanged and debounceTime', fakeAsync(() => {
    fixture.componentInstance.itemsPerPage = 2;
    fixture.componentInstance.totalNumber = 4;
    fixture.componentInstance.page = 1;
    fixture.detectChanges();
    const emittedValues = [];
    fixture.componentInstance.itemsPerPageChange.subscribe(value => emittedValues.push(value));
    page.setItemsPerPage('5');
    tick(400);
    page.setItemsPerPage('6');
    tick(399);
    page.setItemsPerPage('7');
    tick(400);
    page.setItemsPerPage('8');
    page.setItemsPerPage('7');
    tick(400);
    page.setItemsPerPage('0');
    tick(400);

    expect(emittedValues).toEqual([5, 7]);
  }));

  it('should emit pageChange', () => {
    fixture.componentInstance.itemsPerPage = 2;
    fixture.componentInstance.totalNumber = 3;
    fixture.componentInstance.page = 1;
    const emittedValues = [];
    fixture.componentInstance.pageChange.subscribe(value => emittedValues.push(value));
    fixture.detectChanges();
    page.nextPageButton.click();
    fixture.componentInstance.page = 2;
    expect(page.paginationInfo.textContent.trim()).toEqual('1 - 2 of 3');
    fixture.detectChanges();
    page.previousPageButton.click();
    fixture.detectChanges();
    expect(page.paginationInfo.textContent.trim()).toEqual('3 - 3 of 3');
    expect(emittedValues).toEqual([2, 1]);
  });
});

class PageObject {
  constructor(private fixture: ComponentFixture<TestHostComponent>) {}

  get areSwitchIconsVisible(): boolean {
    return this.fixture.nativeElement.querySelectorAll('.pagination__icons').length > 0;
  }

  get previousPageButton(): HTMLButtonElement {
    return this.fixture.nativeElement.querySelector('#previous-page-btn');
  }

  get nextPageButton(): HTMLButtonElement {
    return this.fixture.nativeElement.querySelector('#next-page-btn');
  }

  get itemsPerPageInput(): HTMLInputElement {
    return this.fixture.nativeElement.querySelector('#items-per-page');
  }

  get paginationInfo(): HTMLSpanElement {
    return this.fixture.nativeElement.querySelector('.pagination__info');
  }

  setItemsPerPage(value: string) {
    this.itemsPerPageInput.value = value;
    this.itemsPerPageInput.dispatchEvent(new Event('input'));
    this.fixture.detectChanges();
  }
}

@Component({
  template: `
    <app-pagination
      [itemsPerPage]="itemsPerPage"
      [page]="page"
      [totalNumber]="totalNumber"
      (itemsPerPageChange)="itemsPerPageChange.emit($event)"
      (pageChange)="pageChange.emit($event)"
    ></app-pagination>
  `
})
class TestHostComponent {
  itemsPerPage: number;
  page: number;
  totalNumber: number;
  @Output() itemsPerPageChange = new EventEmitter<number>();
  @Output() pageChange = new EventEmitter<number>();
}
