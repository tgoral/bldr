import { MoviesSortBy } from './enums';
import {
  Actor,
  Actors,
  AuthToken,
  LoginCredentials,
  Movie,
  Movies,
  Pagination,
  MoviesPagination
} from './interfaces';
import { ApiEndpoints } from './types';

// API

export type ApiFindAllEndpoints = {
  '/movies': {
    returnType: Movies;
    queryParamsType: MoviesPagination;
  };
  '/actors': {
    returnType: Actors;
    queryParamsType: Pagination;
  };
};

export type ApiFindByIdEndpoints = {
  '/movies/': {
    returnType: Movie;
  };
  '/actors/': {
    returnType: Actor;
  };
};

export type ApiPostEndpoints = {
  '/auth/login': {
    payload: LoginCredentials;
    returnType: AuthToken;
  };
};

export type ApiEndpoints = ApiFindAllEndpoints & ApiFindByIdEndpoints & ApiPostEndpoints;
// helpers

export type ApiPostPayloadType<T extends keyof ApiPostEndpoints> = ApiPostEndpoints[T]['payload'];
export type ApiReturnType<T extends keyof ApiEndpoints> = ApiEndpoints[T]['returnType'];
export type ApiQueryParamsType<
  T extends keyof ApiFindAllEndpoints
> = ApiFindAllEndpoints[T]['queryParamsType'];
