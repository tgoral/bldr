import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { AppIcons } from './../enums';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconComponent {
  @Input() icon: AppIcons;

  constructor() {}
}
