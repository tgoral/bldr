import { SortDir, MoviesSortBy } from './enums';

export interface Actor {
  _id: string;
  imdbId: string;
  name: string;
  birhday: string | Date;
  country: string;
  gender: 'male' | 'female';
  photoUrl: string;
}

export interface Movie {
  _id: string;
  imdbId: string;
  title: string;
  director: string;
  year: number;
  metascore: number;
  actors: [
    {
      imdbId: string;
      name: string;
    }
  ];
  posterUrl: string;
}

export interface Collection<T = any> {
  collection: T[];
  total: number;
}

export interface Pagination<T = {}> {
  limit?: number;
  page?: number;
  sortBy?: T;
  sortDir?: SortDir;
}

export interface Movies extends Collection<Movie> {}
export interface Actors extends Collection<Actor> {}
export interface MoviesPagination extends Pagination<MoviesSortBy> {}

export interface ErrorMessage {
  errorMessage: string | null;
}

export interface LoginCredentials {
  login: string;
  password: string;
}

export interface AuthToken {
  token: string;
}

export interface TableColumn<T extends object = {}> {
  key: keyof T;
  label: string;
}

export interface Sort<T extends string = any> {
  sortBy: T;
  sortDir: SortDir;
}

export interface ActionPayload<T> {
  payload: T;
}
