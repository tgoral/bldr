import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { LayoutFacade } from '../core/services/layout.facade';
import { MoviesFacade } from '../core/services/movies.facade';
import { Movie } from '../shared/interfaces';
import { TableColumn } from './../shared/interfaces';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MoviesComponent implements OnInit {
  movies$ = this.moviesFacade.movies$;
  itemsPerPage$ = this.moviesFacade.limit$;
  totalItems$ = this.moviesFacade.totalMovies$;
  pageNumber$ = this.moviesFacade.pageNumber$;
  sort$ = this.moviesFacade.sort$;
  loading$ = this.layoutFacade.loading$;
  datatableIdxOffset$ = combineLatest(this.pageNumber$, this.itemsPerPage$).pipe(
    map(([pageNumber, itemsPerPage]) => (pageNumber - 1) * itemsPerPage)
  );
  columns: TableColumn<Movie>[] = [
    {
      key: 'title',
      label: 'Title'
    },
    {
      key: 'year',
      label: 'Year'
    },
    {
      key: 'metascore',
      label: 'Metascore'
    }
  ];

  constructor(private moviesFacade: MoviesFacade, private layoutFacade: LayoutFacade) {}

  ngOnInit() {
    this.moviesFacade.load();
  }

  onMovieSelect(movie: Movie) {
    this.moviesFacade.selectMovie(movie);
  }

  onPageChange(pageNumber: number) {
    this.moviesFacade.loadFromPage(pageNumber);
  }

  sortBy({ columnKey }) {
    this.moviesFacade.loadWithSortedBy(columnKey);
  }

  onItemsPerPageChange(itemsPerPage: number) {
    this.moviesFacade.loadWithLimit(itemsPerPage);
  }
}
