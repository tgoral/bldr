import { MoviesPagination } from './../../shared/interfaces';
import { Movies } from 'src/app/shared/interfaces';
import * as moviesAction from './movies.actions';
import { MoviesSortBy } from 'src/app/shared/enums';

describe('Movies actions', () => {
  describe('loadMovies', () => {
    it('should create an action', () => {
      const action = moviesAction.loadMovies();
      expect(action.type).toEqual(moviesAction.loadMovies.type);
    });
  });

  describe('loadMoviesSuccess', () => {
    it('should create an action', () => {
      const movies: Movies = {
        collection: [],
        total: 5
      };
      const action = moviesAction.loadMoviesSuccess({ payload: movies });
      expect(action.type).toEqual(moviesAction.loadMoviesSuccess.type);
      expect(action.payload).toEqual(movies);
    });
  });

  describe('loadMoviesError', () => {
    it('should create an action', () => {
      const error = {
        errorMessage: 'test err'
      };
      const action = moviesAction.loadMoviesError({ payload: error });
      expect(action.type).toEqual(moviesAction.loadMoviesError.type);
      expect(action.payload).toEqual(error);
    });
  });

  describe('cachePagination', () => {
    it('should create an action', () => {
      const action = moviesAction.cachePagination();
      expect(action.type).toEqual(moviesAction.cachePagination.type);
    });
  });

  describe('loadCachedPagination', () => {
    it('should create an action', () => {
      const action = moviesAction.loadCachedPagination();
      expect(action.type).toEqual(moviesAction.loadCachedPagination.type);
    });
  });

  describe('paginationLoaded', () => {
    it('should create an action', () => {
      const pagination: MoviesPagination = { limit: 4 };
      const action = moviesAction.paginationLoaded({ payload: { pagination } });
      expect(action.type).toEqual(moviesAction.paginationLoaded.type);
      expect(action.payload).toEqual({ pagination });
    });
  });

  describe('changePage', () => {
    it('should create an action', () => {
      const page = { page: 4 };
      const action = moviesAction.changePage({ payload: page });
      expect(action.type).toEqual(moviesAction.changePage.type);
      expect(action.payload).toEqual(page);
    });
  });

  describe('changeItemsPerPage', () => {
    it('should create an action', () => {
      const limit = { limit: 4 };
      const action = moviesAction.changeItemsPerPage({ payload: limit });
      expect(action.type).toEqual(moviesAction.changeItemsPerPage.type);
      expect(action.payload).toEqual(limit);
    });
  });

  describe('sortData', () => {
    it('should create an action', () => {
      const sortBy = { sortBy: MoviesSortBy.Title };
      const action = moviesAction.sortData({ payload: sortBy });
      expect(action.type).toEqual(moviesAction.sortData.type);
      expect(action.payload).toEqual(sortBy);
    });
  });
});
