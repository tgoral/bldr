import { ErrorMessage } from 'src/app/shared/interfaces';
import { SortDir } from './../../shared/enums';
import { MoviesSortBy } from 'src/app/shared/enums';
import { Movies, MoviesPagination } from './../../shared/interfaces';
import * as moviesAction from './movies.actions';
import { moviesReducer, initState, MoviesState } from './movies.reducer';

describe('Movies reducer', () => {
  it('should return the default state', () => {
    const state = moviesReducer(undefined, {} as any);
    expect(state).toBe(initState);
  });

  it('should set movies', () => {
    const testInitState: MoviesState = {
      collection: [
        {
          _id: '2',
          actors: [
            {
              imdbId: '30',
              name: 'Brad Pitt'
            }
          ],
          director: 'Spielberg',
          imdbId: '30',
          metascore: 60,
          posterUrl: 'url',
          title: 'Szeregowiec Ryan',
          year: 2010
        }
      ],
      total: 2,
      pagination: {},
      errorMessage: 'test error'
    };
    const loadedMovies: Movies = {
      collection: [
        {
          _id: '1',
          actors: [
            {
              imdbId: '20',
              name: 'Brad Pitt'
            }
          ],
          director: 'Spielberg',
          imdbId: '10',
          metascore: 50,
          posterUrl: 'url',
          title: 'E.T.',
          year: 2010
        }
      ],
      total: 4
    };

    const state = moviesReducer(
      testInitState,
      moviesAction.loadMoviesSuccess({ payload: loadedMovies })
    );

    expect(state.total).toBe(loadedMovies.total);
    expect(state.errorMessage).toBeNull();
    expect(state.collection.length).toBe(loadedMovies.collection.length);
    expect(state.collection[0]).toBe(loadedMovies.collection[0]);
  });

  it('should set error message', () => {
    const testInitState: MoviesState = {
      collection: [
        {
          _id: '2',
          actors: [
            {
              imdbId: '30',
              name: 'Brad Pitt'
            }
          ],
          director: 'Spielberg',
          imdbId: '30',
          metascore: 60,
          posterUrl: 'url',
          title: 'Szeregowiec Ryan',
          year: 2010
        }
      ],
      total: 2,
      pagination: { limit: 4, page: 2 },
      errorMessage: null
    };
    const error: ErrorMessage = { errorMessage: 'test error' };
    const state = moviesReducer(testInitState, moviesAction.loadMoviesError({ payload: error }));
    expect(state.total).toBe(2);
    expect(state.collection).toBe(testInitState.collection);
    expect(state.pagination).toBe(testInitState.pagination);
    expect(state.errorMessage).toBe(error.errorMessage);
  });

  it('should change page', () => {
    const testInitState: MoviesState = {
      collection: [],
      total: 0,
      pagination: { page: 2, limit: 3 },
      errorMessage: null
    };
    const state = moviesReducer(testInitState, moviesAction.changePage({ payload: { page: 1 } }));
    expect(state.pagination.page).toBe(1);
  });

  it('should set pagination', () => {
    const testInitState: MoviesState = {
      collection: [],
      total: 0,
      pagination: { page: 1, limit: 5 },
      errorMessage: null
    };
    const loadedPagination: MoviesPagination = {
      sortBy: MoviesSortBy.Year,
      sortDir: SortDir.Desc,
      limit: 4
    };
    const state = moviesReducer(
      testInitState,
      moviesAction.paginationLoaded({ payload: { pagination: loadedPagination } })
    );
    expect(state.pagination.page).toBe(1);
    expect(state.pagination.sortBy).toBe(loadedPagination.sortBy);
    expect(state.pagination.sortDir).toBe(loadedPagination.sortDir);
    expect(state.pagination.limit).toBe(loadedPagination.limit);
  });

  it('should change limit', () => {
    const testInitState: MoviesState = {
      collection: [],
      total: 0,
      pagination: { page: 2, limit: 3 },
      errorMessage: null
    };
    const state = moviesReducer(
      testInitState,
      moviesAction.changeItemsPerPage({ payload: { limit: 10 } })
    );
    expect(state.pagination.limit).toBe(10);
  });

  it('should sort data asc when no sorting', () => {
    const testInitState: MoviesState = {
      collection: [],
      total: 0,
      pagination: { page: 2, limit: 3 },
      errorMessage: null
    };
    const state = moviesReducer(
      testInitState,
      moviesAction.sortData({ payload: { sortBy: MoviesSortBy.Title } })
    );
    expect(state.pagination.sortBy).toBe(MoviesSortBy.Title);
    expect(state.pagination.sortDir).toBe(SortDir.Asc);
  });

  it('should sort data asc when sorting by changed', () => {
    const testInitState: MoviesState = {
      collection: [],
      total: 0,
      pagination: { page: 2, limit: 3, sortBy: MoviesSortBy.Metacode, sortDir: SortDir.Asc },
      errorMessage: null
    };
    const state = moviesReducer(
      testInitState,
      moviesAction.sortData({ payload: { sortBy: MoviesSortBy.Title } })
    );
    expect(state.pagination.sortBy).toBe(MoviesSortBy.Title);
    expect(state.pagination.sortDir).toBe(SortDir.Asc);
  });

  it('should sort data desc', () => {
    const testInitState: MoviesState = {
      collection: [],
      total: 0,
      pagination: { page: 2, limit: 3, sortBy: MoviesSortBy.Metacode, sortDir: SortDir.Asc },
      errorMessage: null
    };
    const state = moviesReducer(
      testInitState,
      moviesAction.sortData({ payload: { sortBy: MoviesSortBy.Metacode } })
    );
    expect(state.pagination.sortBy).toBe(MoviesSortBy.Metacode);
    expect(state.pagination.sortDir).toBe(SortDir.Desc);
  });
});
