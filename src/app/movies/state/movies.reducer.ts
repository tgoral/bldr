import { createReducer, on, Action } from '@ngrx/store';
import { Movies, ErrorMessage } from 'src/app/shared/interfaces';
import { AppState } from './../../core/reducers/index';
import { SortDir } from './../../shared/enums';
import { MoviesPagination } from './../../shared/interfaces';
import {
  changeItemsPerPage,
  changePage,
  loadMoviesSuccess,
  paginationLoaded,
  sortData,
  loadMoviesError
} from './movies.actions';

export type MoviesState = Movies & { pagination: MoviesPagination } & ErrorMessage;

export interface State extends AppState {
  movies: MoviesState;
}

export const initState: MoviesState = {
  collection: [],
  total: 0,
  pagination: { page: 1, limit: 5 },
  errorMessage: null
};

const reducer = createReducer(
  initState,
  on(loadMoviesSuccess, (state, action) => ({
    ...state,
    collection: [...action.payload.collection],
    total: action.payload.total,
    errorMessage: null
  })),
  on(loadMoviesError, (state, action) => ({
    ...state,
    errorMessage: action.payload.errorMessage
  })),
  on(changePage, (state, action) => ({
    ...state,
    pagination: { ...state.pagination, page: action.payload.page }
  })),
  on(paginationLoaded, (state, action) => ({
    ...state,
    pagination: { ...state.pagination, ...action.payload.pagination }
  })),
  on(changeItemsPerPage, (state, action) => ({
    ...state,
    pagination: { ...state.pagination, limit: action.payload.limit, page: 1 }
  })),
  on(sortData, (state, action) => {
    let sortDir = SortDir.Asc;
    if (
      state.pagination.sortBy === action.payload.sortBy &&
      state.pagination.sortDir === SortDir.Asc
    ) {
      sortDir = SortDir.Desc;
    }
    return {
      ...state,
      pagination: { ...state.pagination, sortDir, sortBy: action.payload.sortBy, page: 1 }
    };
  })
);

export function moviesReducer(state: MoviesState, action: Action) {
  return reducer(state, action);
}
