import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable, of, throwError } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { HttpService } from './../../core/services/http.service';
import { Movies, MoviesPagination } from './../../shared/interfaces';
import * as moviesAction from './movies.actions';
import { MoviesEffects } from './movies.effects';
import * as fromMovies from './movies.reducer';

describe('MoviesEffects', () => {
  let effects: MoviesEffects;
  let httpService: jasmine.SpyObj<HttpService>;
  let localStorage: jasmine.SpyObj<LocalStorageService>;
  let actions$: Observable<any>;
  let store: MockStore<fromMovies.State>;
  const initialState = {
    movies: {
      collection: [],
      total: 10,
      pagination: {},
      errorMessage: null
    }
  } as fromMovies.State;

  let testScheduler;

  beforeEach(() => {
    httpService = jasmine.createSpyObj('httpService', ['findAll']);
    localStorage = jasmine.createSpyObj('localStorage', ['getPagination', 'setPagination']);
    TestBed.configureTestingModule({
      providers: [
        MoviesEffects,
        { provide: HttpService, useValue: httpService },
        { provide: LocalStorageService, useValue: localStorage },
        provideMockActions(() => actions$),
        provideMockStore({ initialState })
      ]
    });
    httpService = TestBed.get(HttpService);
    localStorage = TestBed.get(LocalStorageService);
    actions$ = TestBed.get(Actions);
    store = TestBed.get(Store);
    effects = TestBed.get(MoviesEffects);
    testScheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });
  });

  describe('loadMovies$', () => {
    it('should load movies success', () => {
      testScheduler.run(({ hot, expectObservable }) => {
        const initAction = moviesAction.loadMovies();
        const movies: Movies = {
          collection: [
            {
              _id: '2',
              actors: [
                {
                  imdbId: '30',
                  name: 'Brad Pitt'
                }
              ],
              director: 'Spielberg',
              imdbId: '30',
              metascore: 60,
              posterUrl: 'url',
              title: 'Szeregowiec Ryan',
              year: 2010
            }
          ],
          total: 2
        };
        const loadMoviesSuccess = moviesAction.loadMoviesSuccess({ payload: movies });
        const cachePagination = moviesAction.cachePagination();
        httpService.findAll
          .withArgs('/movies', initialState.movies.pagination)
          .and.returnValue(of(movies));
        const values = {
          a: initAction,
          s: loadMoviesSuccess,
          c: cachePagination
        };
        actions$ = hot('-a', values);
        const expected = '-(sc)';
        expectObservable(effects.loadMovies$).toBe(expected, values);
      });
    });

    it('should return load movies error', () => {
      testScheduler.run(({ hot, expectObservable }) => {
        const initAction = moviesAction.loadMovies();
        const error = {
          message: 'test err'
        };
        const errorAction = moviesAction.loadMoviesError({ payload: { errorMessage: error.message } });
        httpService.findAll
          .withArgs('/movies', initialState.movies.pagination)
          .and.returnValue(throwError(error));
        const values = {
          a: initAction,
          e: errorAction
        };
        actions$ = hot('-a', values);
        const expected = '-e';
        expectObservable(effects.loadMovies$).toBe(expected, values);
      });
    });
  });

  describe('cachePagination$', () => {
    it('should save pagination to local storage', () => {
      testScheduler.run(({ hot }) => {
        const initAction = moviesAction.cachePagination();
        const values = {
          a: initAction
        };
        actions$ = hot('-a', values);
        effects.cachePagination$.subscribe(() => {
          expect(localStorage.setPagination).toHaveBeenCalledWith(initialState.movies.pagination);
        });
      });
    });
  });

  describe('loadCachedPagination$', () => {
    it('should load cached pagination', () => {
      testScheduler.run(({ hot, expectObservable }) => {
        const pagination: MoviesPagination = { limit: 2 };
        const initAction = moviesAction.loadCachedPagination();
        const successAction = moviesAction.paginationLoaded({ payload: { pagination } });
        localStorage.getPagination.and.returnValue(pagination);
        const values = {
          a: initAction,
          s: successAction
        };
        actions$ = hot('-a', values);
        const expected = '-s';
        expectObservable(effects.loadCachedPagination$).toBe(expected, values);
      });
    });
  });
});
