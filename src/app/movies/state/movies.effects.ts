import { Injectable } from '@angular/core';
import { Actions, Effect, ofType, createEffect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { concatMap, map, switchMap, tap, catchError, withLatestFrom } from 'rxjs/operators';
import { HttpService } from './../../core/services/http.service';
import { LocalStorageService } from './../../core/services/local-storage.service';
import {
  cachePagination,
  loadCachedPagination,
  loadMovies,
  loadMoviesSuccess,
  paginationLoaded,
  loadMoviesError
} from './movies.actions';
import { State } from './movies.reducer';
import { selectPagination } from './movies.selectors';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';

@Injectable()
export class MoviesEffects {
  loadMovies$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadMovies),
      withLatestFrom(this.store.select(selectPagination)),
      switchMap(([action, pagination]) =>
        this.httpService.findAll('/movies', pagination).pipe(
          concatMap(movies => [loadMoviesSuccess({ payload: movies }), cachePagination()]),
          catchError((error: HttpErrorResponse) =>
            of(loadMoviesError({ payload: { errorMessage: error.message } }))
          )
        )
      )
    )
  );

  cachePagination$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(cachePagination),
        withLatestFrom(this.store.select(selectPagination)),
        tap(([action, pagination]) => this.localStorage.setPagination(pagination))
      ),
    { dispatch: false }
  );

  loadCachedPagination$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadCachedPagination),
      map(() => {
        const pagination = this.localStorage.getPagination();
        const action = paginationLoaded({ payload: { pagination } });
        return action;
      })
    )
  );

  constructor(
    private actions$: Actions,
    private httpService: HttpService,
    private store: Store<State>,
    private localStorage: LocalStorageService
  ) {}
}
