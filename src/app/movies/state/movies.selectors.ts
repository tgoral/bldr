import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State, MoviesState } from './movies.reducer';

export const selectMoviesState = createFeatureSelector<State, MoviesState>('movies');

export const selectMoviesCollection = createSelector(
  selectMoviesState,
  movies => movies.collection
);
export const selectPagination = createSelector(
  selectMoviesState,
  movies => movies.pagination
);
export const selectItemsPerPage = createSelector(
  selectPagination,
  pagination => pagination.limit
);
export const selectTotalNumber = createSelector(
  selectMoviesState,
  movies => movies.total
);
export const selectPageNumber = createSelector(
  selectPagination,
  pagination => pagination.page
);
export const selectSorting = createSelector(
  selectPagination,
  pagination => ({ sortBy: pagination.sortBy, sortDir: pagination.sortDir })
);

export const selectMoviesErrorMessage = createSelector(
  selectMoviesState,
  movies => movies.errorMessage
);
