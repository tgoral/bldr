import { ActionPayload, ErrorMessage } from './../../shared/interfaces';
import { MoviesPagination } from './../../shared/interfaces';
import { Movies } from 'src/app/shared/interfaces';
import { createAction, props } from '@ngrx/store';

export const loadMovies = createAction('[Movies] Load movies');
export const loadMoviesSuccess = createAction(
  '[Movies] Load movies success',
  props<ActionPayload<Movies>>()
);
export const loadMoviesError = createAction(
  '[Movies] Load movies error',
  props<ActionPayload<ErrorMessage>>()
);

export const cachePagination = createAction('[Pagination] Cache pagination');
export const loadCachedPagination = createAction('[Pagination] Load cached pagination');
export const paginationLoaded = createAction(
  '[Pagination] Pagination loaded',
  props<ActionPayload<{ pagination: MoviesPagination }>>()
);
export const changePage = createAction(
  '[Pagination] Change page',
  props<ActionPayload<Pick<MoviesPagination, 'page'>>>()
);
export const changeItemsPerPage = createAction(
  '[Pagination] Change items per page',
  props<ActionPayload<Pick<MoviesPagination, 'limit'>>>()
);
export const sortData = createAction(
  '[Pagination] Sort data',
  props<ActionPayload<Pick<MoviesPagination, 'sortBy'>>>()
);
