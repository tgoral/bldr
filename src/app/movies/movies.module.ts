import { MoviesComponent } from './movies.component';
import { EffectsModule } from '@ngrx/effects';
import { SharedModule } from './../shared/shared.module';
import { MoviesRoutingModule } from './movies-routing.module';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { moviesReducer } from './state/movies.reducer';
import { MoviesEffects } from './state/movies.effects';

@NgModule({
  declarations: [MoviesComponent],
  imports: [
    MoviesRoutingModule,
    SharedModule,
    StoreModule.forFeature('movies', moviesReducer),
    EffectsModule.forFeature([MoviesEffects])
  ]
})
export class MoviesModule {}
