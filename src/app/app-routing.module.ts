import { IsAuthenticatedGuard } from './core/guards/is-authenitcated.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
    data: { animation: 'login' }
  },
  {
    path: 'movies/:id',
    loadChildren: () => import('./movie/movie.module').then(m => m.MovieModule),
    canActivate: [IsAuthenticatedGuard],
    data: { animation: 'movie' }
  },
  {
    path: 'movies',
    loadChildren: () => import('./movies/movies.module').then(m => m.MoviesModule),
    canActivate: [IsAuthenticatedGuard],
    data: { animation: 'movies' }
  },
  { path: '**', redirectTo: 'movies' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
