import { createReducer, on, Action } from '@ngrx/store';
import { loginSuccess, loginFailure, invalidToken } from '../actions/auth.actions';

export type AuthState = {
  isAuthenticated: boolean;
  errorMessage: string | null;
};

export const initState: AuthState = {
  isAuthenticated: false,
  errorMessage: null
};

const reducer = createReducer(
  initState,
  on(loginSuccess, () => ({ isAuthenticated: true, errorMessage: null })),
  on(loginFailure, invalidToken, (state, action) => ({
    isAuthenticated: false,
    errorMessage: action.payload.errorMessage
  }))
);

export function authReducer(state: AuthState, action: Action) {
  return reducer(state, action);
}
