import { ActorsState } from './actors.reducer';
import { Actors } from './../../shared/interfaces';
import { createReducer, on, Action } from '@ngrx/store';
import { actorsLoaded } from '../actions/actors.actions';

export type ActorsState = Actors;

export const initState: ActorsState = {
  collection: [],
  total: 0
};

const reducer = createReducer(
  initState,
  on(actorsLoaded, (state, { payload: { actors } }) => ({
    collection: [...actors.collection],
    total: actors.total
  }))
);

export function actorsReducer(state: ActorsState, action: Action) {
  return reducer(state, action);
}
