import { LayoutState, layoutReducer } from './layout.reducer';
import { AuthState, authReducer } from './auth.reducer';
import { ActorsState, actorsReducer } from './actors.reducer';
import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  ActionReducer
} from '@ngrx/store';
import { logout } from '../actions/auth.actions';

export interface AppState {
  actors: ActorsState;
  auth: AuthState;
  layout: LayoutState;
}

export const reducers: ActionReducerMap<AppState> = {
  actors: actorsReducer,
  auth: authReducer,
  layout: layoutReducer
};

export function metaReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state, action) => {
    if (action.type === logout().type) {
      return reducer(undefined, action);
    }
    return reducer(state, action);
  };
}

export const metaReducers = [metaReducer];

export const selectActorsState = createFeatureSelector<ActorsState>('actors');
export const selectAuthState = createFeatureSelector<AuthState>('auth');
export const selectLayoutState = createFeatureSelector<LayoutState>('layout');

export const selectActor = (id: string) =>
  createSelector(
    selectActorsState,
    actors => actors.collection.find(actor => actor.imdbId === id)
  );

export const selectIsAuthenticated = createSelector(
  selectAuthState,
  auth => auth.isAuthenticated
);

export const selectIsLoading = createSelector(
  selectLayoutState,
  layout => layout.loading
);

export const selectAuthErrorMessage = createSelector(
  selectAuthState,
  auth => auth.errorMessage
);
