import * as layoutAction from './../actions/layout.actions';
import { layoutReducer, initState, LayoutState } from './layout.reducer';

describe('Layout reducer', () => {
  it('should return the default state', () => {
    const state = layoutReducer(undefined, {} as any);
    expect(state).toBe(initState);
  });

  it('should set loading', () => {
    const testInitState: LayoutState = {
      loading: false
    };
    const state = layoutReducer(testInitState, layoutAction.loading());
    expect(state.loading).toBeTruthy();
  });

  it('should set not loading', () => {
    const testInitState: LayoutState = {
      loading: true
    };
    const state = layoutReducer(testInitState, layoutAction.loaded());
    expect(state.loading).toBeFalsy();
  });
});
