import { Actors, Actor } from './../../shared/interfaces';
import * as root from './index';

describe('Actors selectors', () => {
  const actor1: Actor = {
    _id: '1',
    birhday: '1995',
    country: 'Poland',
    gender: 'male',
    imdbId: '10',
    name: 'Bogusław Linda',
    photoUrl: 'url'
  };
  const actor2: Actor = {
    _id: '10',
    birhday: '1994',
    country: 'USA',
    gender: 'male',
    imdbId: '1',
    name: 'Brad Pitt',
    photoUrl: 'url'
  };
  const mockActors: Actors = {
    collection: [actor1, actor2],
    total: 5
  };

  describe('selectActor', () => {
    it('should select actor by imdbId', () => {
      expect(root.selectActor('1').projector(mockActors)).toEqual(actor2);
    });

    it('should return undefined when no actor with given imdbId', () => {
      expect(root.selectActor('100').projector(mockActors)).toBeUndefined();
    });
  });
});
