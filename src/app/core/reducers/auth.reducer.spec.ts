import { authReducer, initState, AuthState } from './auth.reducer';
import * as authAction from './../actions/auth.actions';

describe('Auth reducer', () => {
  it('should return the default state', () => {
    const state = authReducer(undefined, {} as any);
    expect(state).toBe(initState);
  });

  it('should set authenticated and remove error message', () => {
    const testInitState: AuthState = {
      isAuthenticated: false,
      errorMessage: 'error'
    };
    const state = authReducer(testInitState, authAction.loginSuccess());
    expect(state.errorMessage).toBeNull();
    expect(state.isAuthenticated).toBeTruthy();
  });

  it('should set not authenticated and add error message when login failure', () => {
    const testInitState: AuthState = {
      isAuthenticated: true,
      errorMessage: null
    };
    const errorMessage = 'error';
    const state = authReducer(
      testInitState,
      authAction.loginFailure({ payload: { errorMessage } })
    );
    expect(state.errorMessage).toBe(errorMessage);
    expect(state.isAuthenticated).toBeFalsy();
  });

  it('should set not authenticated and add error message when invalid token', () => {
    const testInitState: AuthState = {
      isAuthenticated: true,
      errorMessage: null
    };
    const errorMessage = 'error';
    const state = authReducer(
      testInitState,
      authAction.invalidToken({ payload: { errorMessage } })
    );
    expect(state.errorMessage).toBe(errorMessage);
    expect(state.isAuthenticated).toBeFalsy();
  });
});
