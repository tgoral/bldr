import { loaded } from './../actions/layout.actions';
import { createReducer, on, Action } from '@ngrx/store';
import { loading } from '../actions/layout.actions';

export interface LayoutState {
  loading: boolean;
}

export const initState: LayoutState = {
  loading: false
};

const reducer = createReducer(
  initState,
  on(loading, state => ({ ...state, loading: true })),
  on(loaded, state => ({ ...state, loading: false }))
);

export function layoutReducer(state: LayoutState, action: Action) {
  return reducer(state, action);
}
