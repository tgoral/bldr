import { Actors } from './../../shared/interfaces';
import * as actorAction from './../actions/actors.actions';
import { actorsReducer, initState } from './actors.reducer';

describe('Actors reducer', () => {
  it('should return the default state', () => {
    const state = actorsReducer(undefined, {} as any);
    expect(state).toBe(initState);
  });

  it('should set new state', () => {
    const testInitState: Actors = {
      collection: [
        {
          _id: '1',
          birhday: '1995',
          country: 'Poland',
          gender: 'male',
          imdbId: '1',
          name: 'Brad Pitt',
          photoUrl: 'url'
        }
      ],
      total: 5
    };
    const loadedActors: Actors = {
      collection: [
        {
          _id: '4',
          birhday: '1994',
          country: 'Poland',
          gender: 'male',
          imdbId: '10',
          name: 'Bogusław Linda',
          photoUrl: 'url'
        },
        {
          _id: '5',
          birhday: '1993',
          country: 'Poland',
          gender: 'male',
          imdbId: '10',
          name: 'Cezary Pazura',
          photoUrl: 'url'
        }
      ],
      total: 3
    };
    const action = actorAction.actorsLoaded({ payload: { actors: loadedActors } });
    const state = actorsReducer(testInitState, action);
    expect(state.total).toBe(loadedActors.total);
    expect(state.collection.length).toBe(loadedActors.collection.length);
    expect(state.collection[0]).toBe(loadedActors.collection[0]);
    expect(state.collection[1]).toBe(loadedActors.collection[1]);
  });
});
