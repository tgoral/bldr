import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject } from 'rxjs';
import { AuthFacade } from '../services/auth.facade';
import { SharedModule } from './../../shared/shared.module';
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let authFacade: jasmine.SpyObj<AuthFacade>;
  let fixture: ComponentFixture<HeaderComponent>;
  let page: PageObject;
  let isAuthenticated$: BehaviorSubject<boolean>;

  beforeEach(() => {
    isAuthenticated$ = new BehaviorSubject(false);
    authFacade = jasmine.createSpyObj('authFacade', ['logout', 'isAuthenticated']);
    authFacade.isAuthenticated.and.returnValue(isAuthenticated$);
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      imports: [SharedModule, RouterTestingModule.withRoutes([])],
      providers: [{ provide: AuthFacade, useValue: authFacade }]
    });

    fixture = TestBed.createComponent(HeaderComponent);
    fixture.detectChanges();
    authFacade = TestBed.get(AuthFacade);
    page = new PageObject(fixture);
  });

  it('should show nav only when is authenticated', () => {
    expect(page.isNavVisible).toBeFalsy();
    isAuthenticated$.next(true);
    fixture.detectChanges();
    expect(page.isNavVisible).toBeTruthy();
  });

  it('should call authFacade when logout link clicked', () => {
    isAuthenticated$.next(true);
    fixture.detectChanges();
    page.logoutLink.click();
    fixture.detectChanges();
    expect(authFacade.logout).toHaveBeenCalled();
  });
});

class PageObject {
  constructor(private fixture: ComponentFixture<HeaderComponent>) {}

  get logoutLink(): HTMLLinkElement {
    return this.fixture.nativeElement.querySelector('.nav__logout');
  }

  get isNavVisible(): boolean {
    return this.fixture.nativeElement.querySelectorAll('nav').length > 0;
  }
}
