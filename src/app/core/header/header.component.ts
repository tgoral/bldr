import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AuthFacade } from '../services/auth.facade';
import { AppIcons } from './../../shared/enums';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {
  isAuthenticated$ = this.authService.isAuthenticated();
  icons = AppIcons;

  constructor(private authService: AuthFacade) {}

  logout() {
    this.authService.logout();
  }
}
