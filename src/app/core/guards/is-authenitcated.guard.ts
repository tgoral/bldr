import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { authenticated, invalidToken } from '../actions/auth.actions';
import { AppState, selectIsAuthenticated } from '../reducers';
import { AuthFacade } from '../services/auth.facade';

@Injectable()
export class IsAuthenticatedGuard implements CanActivate {
  constructor(
    private authService: AuthFacade,
    private router: Router,
    private store: Store<AppState>
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.store.select(selectIsAuthenticated).pipe(
      map(isAuthenticated => {
        const isTokenValid = this.authService.isTokenValid();
        if (!isAuthenticated && isTokenValid) {
          this.store.dispatch(authenticated());
        }
        return isTokenValid;
      }),
      catchError(err => {
        this.store.dispatch(
          invalidToken({ payload: { errorMessage: 'Authentication token is invalid!' } })
        );
        return of(false);
      }),
      tap(isAuthenticated => {
        if (!isAuthenticated) {
          this.router.navigate(['login']);
        }
      })
    );
  }
}
