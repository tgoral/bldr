import { authenticated, invalidToken } from './../actions/auth.actions';
import { IsAuthenticatedGuard } from './is-authenitcated.guard';
import { Router } from '@angular/router';
import { AuthFacade } from '../services/auth.facade';
import { Store } from '@ngrx/store';
import { AppState, selectIsAuthenticated } from '../reducers';
import { of } from 'rxjs';

describe('IsAuthenticatedGuard', () => {
  let authService: jasmine.SpyObj<AuthFacade>;
  let router: jasmine.SpyObj<Router>;
  let store: jasmine.SpyObj<Store<AppState>>;
  let isAuthenticatedGuard: IsAuthenticatedGuard;

  beforeEach(() => {
    authService = jasmine.createSpyObj('authService', ['isTokenValid']);
    router = jasmine.createSpyObj('router', ['navigate']);
    store = jasmine.createSpyObj('store', ['dispatch', 'select']);
    isAuthenticatedGuard = new IsAuthenticatedGuard(authService, router, store);
  });

  it('should return true when isAuthenticated and token not expired', () => {
    store.select.withArgs(selectIsAuthenticated).and.returnValue(of(true));
    authService.isTokenValid.and.returnValue(true);

    isAuthenticatedGuard.canActivate(null, null).subscribe(canActivate => {
      expect(canActivate).toBeTruthy();
      expect(store.dispatch).not.toHaveBeenCalled();
      expect(router.navigate).not.toHaveBeenCalled();
    });
  });

  it('should return true and dispatch authenticated when token from localStorage is valid', () => {
    store.select.withArgs(selectIsAuthenticated).and.returnValue(of(false));
    authService.isTokenValid.and.returnValue(true);

    isAuthenticatedGuard.canActivate(null, null).subscribe(canActivate => {
      expect(canActivate).toBeTruthy();
      expect(store.dispatch).toHaveBeenCalledWith(authenticated());
      expect(router.navigate).not.toHaveBeenCalled();
    });
  });

  it('should return false and navigate to login when token expired', () => {
    store.select.withArgs(selectIsAuthenticated).and.returnValue(of(true));
    authService.isTokenValid.and.returnValue(false);

    isAuthenticatedGuard.canActivate(null, null).subscribe(canActivate => {
      expect(canActivate).toBeFalsy();
      expect(store.dispatch).not.toHaveBeenCalled();
      expect(router.navigate).toHaveBeenCalledWith(['login']);
    });
  });

  it('should return false and dispatch action invalidToken', () => {
    store.select.withArgs(selectIsAuthenticated).and.returnValue(of(true));
    authService.isTokenValid.and.throwError('error');

    isAuthenticatedGuard.canActivate(null, null).subscribe(canActivate => {
      expect(canActivate).toBeFalsy();
      expect(store.dispatch).toHaveBeenCalledWith(
        invalidToken({ payload: { errorMessage: 'Authentication token is invalid!' } })
      );
      expect(router.navigate).toHaveBeenCalledWith(['login']);
    });
  });
});
