import { of, ReplaySubject } from 'rxjs';
import { loadActors } from '../actions/actors.actions';
import { Actors } from './../../shared/interfaces';
import { HttpService } from './../services/http.service';
import { ActorsEffects } from './actors.effects';

describe('ActorsEffects', () => {
  let httpService: jasmine.SpyObj<HttpService>;

  beforeEach(() => {
    httpService = jasmine.createSpyObj('httpService', ['findAll']);
  });

  describe('loadActors', () => {
    it('should dispatch actorsLoaded', () => {
      const actions: ReplaySubject<any> = new ReplaySubject(1);
      actions.next(loadActors());
      const actorsEffects = new ActorsEffects(actions, httpService);
      const actors: Actors = {
        collection: [],
        total: 5
      };
      httpService.findAll.withArgs('/actors').and.returnValue(of(actors));
      actorsEffects.loadActors$.subscribe(result => {
        expect(result.payload.actors).toBe(actors);
      });
    });
  });
});
