import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';
import { actorsLoaded, loadActors } from './../actions/actors.actions';
import { HttpService } from './../services/http.service';

@Injectable()
export class ActorsEffects {
  loadActors$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadActors),
      mergeMap(() => this.httpService.findAll('/actors')),
      map(actors => actorsLoaded({ payload: { actors } }))
    )
  );

  constructor(private actions$: Actions, private httpService: HttpService) {}
}
