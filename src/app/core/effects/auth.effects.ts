import { loadCachedPagination } from './../../movies/state/movies.actions';
import { HttpService } from './../services/http.service';
import { loginFailure, authenticated } from './../actions/auth.actions';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType, createEffect } from '@ngrx/effects';
import { login, loginSuccess } from '../actions/auth.actions';
import { switchMap, concatMap, tap, catchError, mergeMap } from 'rxjs/operators';
import { loadActors } from '../actions/actors.actions';
import { Router } from '@angular/router';
import { TypedAction } from '@ngrx/store/src/models';
import { LocalStorageService } from '../services/local-storage.service';
import { of } from 'rxjs';

@Injectable()
export class AuthEffects {
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(login),
      switchMap(({ payload }) =>
        this.httpService.post('/auth/login', payload).pipe(
          tap(token => {
            this.localStorage.setToken(token);
            this.router.navigate(['movies']);
          }),
          concatMap(() => this.authSuccessActions()),
          catchError(() =>
            of(loginFailure({ payload: { errorMessage: 'Authentication failed!' } }))
          )
        )
      )
    )
  );

  authenticated$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authenticated),
      mergeMap(() => this.authSuccessActions())
    )
  );

  constructor(
    private actions$: Actions,
    private localStorage: LocalStorageService,
    private router: Router,
    private httpService: HttpService
  ) {}

  private authSuccessActions(): TypedAction<string>[] {
    return [loginSuccess(), loadActors(), loadCachedPagination()];
  }
}
