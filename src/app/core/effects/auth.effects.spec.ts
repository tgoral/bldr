import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, of, throwError } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { HttpService } from './../../core/services/http.service';
import { loadCachedPagination } from './../../movies/state/movies.actions';
import { AuthToken, LoginCredentials } from './../../shared/interfaces';
import { loadActors } from './../actions/actors.actions';
import * as authAction from './../actions/auth.actions';
import { AuthEffects } from './auth.effects';

describe('AuthEffects', () => {
  let effects: AuthEffects;
  let httpService: jasmine.SpyObj<HttpService>;
  let localStorage: jasmine.SpyObj<LocalStorageService>;
  let router: jasmine.SpyObj<Router>;
  let actions$: Observable<any>;

  let testScheduler;

  beforeEach(() => {
    httpService = jasmine.createSpyObj('httpService', ['post']);
    localStorage = jasmine.createSpyObj('localStorage', ['setToken']);
    router = jasmine.createSpyObj('router', ['navigate']);
    TestBed.configureTestingModule({
      providers: [
        AuthEffects,
        { provide: HttpService, useValue: httpService },
        { provide: LocalStorageService, useValue: localStorage },
        { provide: Router, useValue: router },
        provideMockActions(() => actions$)
      ]
    });
    httpService = TestBed.get(HttpService);
    localStorage = TestBed.get(LocalStorageService);
    router = TestBed.get(Router);
    actions$ = TestBed.get(Actions);
    effects = TestBed.get(AuthEffects);
    testScheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });
  });

  describe('login$', () => {
    it('should login success', () => {
      testScheduler.run(({ hot, expectObservable }) => {
        const credentials: LoginCredentials = {
          login: 'xd@email',
          password: '1234'
        };
        const token: AuthToken = {
          token: 'xdddd'
        };
        const initAction = authAction.login({ payload: credentials });
        const loginSuccessAction = authAction.loginSuccess();
        const loadActorsAction = loadActors();
        const loadCachedPaginationAction = loadCachedPagination();
        httpService.post.withArgs('/auth/login', credentials).and.returnValue(of(token));
        const values = {
          a: initAction,
          s: loginSuccessAction,
          z: loadActorsAction,
          p: loadCachedPaginationAction
        };
        actions$ = hot('-a', values);
        const expected = '-(szp)';
        expectObservable(effects.login$).toBe(expected, values);
        effects.login$.subscribe(() => {
          expect(localStorage.setToken).toHaveBeenCalledWith(token);
          expect(router.navigate).toHaveBeenCalledWith(['movies']);
        });
      });
    });

    it('should login failure', () => {
      testScheduler.run(({ hot, expectObservable }) => {
        const credentials: LoginCredentials = {
          login: 'xd@email',
          password: '1234'
        };
        const error = {
          message: 'error message'
        };
        const initAction = authAction.login({ payload: credentials });
        const loginFailureAction = authAction.loginFailure({
          payload: { errorMessage: 'Authentication failed!' }
        });
        httpService.post.withArgs('/auth/login', credentials).and.returnValue(throwError(error));
        const values = {
          a: initAction,
          e: loginFailureAction
        };
        actions$ = hot('-a', values);
        const expected = '-e';
        expectObservable(effects.login$).toBe(expected, values);
        effects.login$.subscribe(() => {
          expect(localStorage.setToken).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('authenticated$', () => {
    it('should dispatch auth success actions', () => {
      testScheduler.run(({ hot, expectObservable }) => {
        const initAction = authAction.authenticated();
        const loginSuccessAction = authAction.loginSuccess();
        const loadActorsAction = loadActors();
        const loadCachedPaginationAction = loadCachedPagination();
        const values = {
          a: initAction,
          s: loginSuccessAction,
          z: loadActorsAction,
          p: loadCachedPaginationAction
        };
        actions$ = hot('-a', values);
        const expected = '-(szp)';
        expectObservable(effects.authenticated$).toBe(expected, values);
      });
    });
  });
});
