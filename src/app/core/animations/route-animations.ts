import { trigger, transition, style, query, group, animate } from '@angular/animations';

export const routerAnimation = trigger('routeAnimations', [
  transition('* <=> *', [
    query(
      ':enter, :leave',
      style({ position: 'fixed', width: '100%', left: '50%', transform: 'translateX(-25%)' }),
      { optional: true }
    ),
    group([
      query(
        ':enter',
        [
          style({ transform: 'translateX(100%)', opacity: 0 }),
          animate('0.3s ease-in', style({ transform: 'translateX(0%)', opacity: 1 }))
        ],
        { optional: true }
      ),
      query(
        ':leave',
        [
          style({ transform: 'translateX(0%)', opacity: 1 }),
          animate('0.3s ease-in', style({ transform: 'translateX(-100%)', opacity: 0 }))
        ],
        { optional: true }
      )
    ])
  ])
]);
