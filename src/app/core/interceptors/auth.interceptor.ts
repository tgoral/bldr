import { Store } from '@ngrx/store';
import { LocalStorageService } from './../services/local-storage.service';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { AppState } from '../reducers';
import { loading, loaded } from '../actions/layout.actions';
import { finalize } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private numberOfProcessingRequest = 0;

  constructor(private localStorage: LocalStorageService, private store: Store<AppState>) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    this.store.dispatch(loading());
    this.numberOfProcessingRequest++;
    const reqWithAuthorization = req.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.localStorage.getToken()
      }
    });
    return next.handle(reqWithAuthorization).pipe(
      finalize(() => {
        this.numberOfProcessingRequest--;
        if (this.numberOfProcessingRequest === 0) {
          this.store.dispatch(loaded());
        }
      })
    );
  }
}
