import { LoginCredentials, ActionPayload, ErrorMessage } from './../../shared/interfaces';
import { createAction, props } from '@ngrx/store';

export const login = createAction('[Auth] Login', props<ActionPayload<LoginCredentials>>());
export const loginSuccess = createAction('[Auth] Login success');
export const authenticated = createAction('[Auth] Authenticated by token from storage');
export const loginFailure = createAction(
  '[Auth] Login failure',
  props<ActionPayload<ErrorMessage>>()
);
export const invalidToken = createAction(
  '[Auth] Invalid token',
  props<ActionPayload<ErrorMessage>>()
);
export const logout = createAction('[Auth] Logout');
