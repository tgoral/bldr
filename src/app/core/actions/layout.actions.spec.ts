import * as layoutAction from './layout.actions';

describe('Layout actions', () => {
  describe('loading', () => {
    it('should create an action', () => {
      const action = layoutAction.loading();
      expect(action.type).toEqual(layoutAction.loading.type);
    });
  });

  describe('loaded', () => {
    it('should create an action', () => {
      const action = layoutAction.loaded();
      expect(action.type).toEqual(layoutAction.loaded.type);
    });
  });
});
