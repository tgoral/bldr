import { LoginCredentials } from './../../shared/interfaces';
import * as authAction from './auth.actions';

describe('Auth actions', () => {
  describe('login', () => {
    it('should create an action', () => {
      const credentials: LoginCredentials = {
        login: 'login@email',
        password: '1234'
      };
      const action = authAction.login({ payload: credentials });
      expect(action.payload).toEqual(credentials);
      expect(action.type).toEqual(authAction.login.type);
    });
  });

  describe('loginSuccess', () => {
    it('should create an action', () => {
      const action = authAction.loginSuccess();
      expect(action.type).toEqual(authAction.loginSuccess.type);
    });
  });

  describe('authenticated', () => {
    it('should create an action', () => {
      const action = authAction.authenticated();
      expect(action.type).toEqual(authAction.authenticated.type);
    });
  });

  describe('loginFailure', () => {
    it('should create an action', () => {
      const error = { errorMessage: 'test error' };
      const action = authAction.loginFailure({ payload: error });
      expect(action.payload).toEqual(error);
      expect(action.type).toEqual(authAction.loginFailure.type);
    });
  });

  describe('invalidToken', () => {
    it('should create an action', () => {
      const error = { errorMessage: 'test error' };
      const action = authAction.invalidToken({ payload: error });
      expect(action.payload).toEqual(error);
      expect(action.type).toEqual(authAction.invalidToken.type);
    });
  });

  describe('logout', () => {
    it('should create an action', () => {
      const action = authAction.logout();
      expect(action.type).toEqual(authAction.logout.type);
    });
  });
});
