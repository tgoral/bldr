import { createAction } from '@ngrx/store';

export const loading = createAction('[Layout] Loading');
export const loaded = createAction('[Layout] Loaded');
