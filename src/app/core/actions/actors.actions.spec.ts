import { Actors } from './../../shared/interfaces';
import * as actorsAction from './actors.actions';

describe('Actors actions', () => {
  describe('loadActors', () => {
    it('should create an action', () => {
      const action = actorsAction.loadActors();
      expect(action.type).toEqual(actorsAction.loadActors.type);
    });
  });

  describe('actorsLoaded', () => {
    it('should create an action', () => {
      const actors: Actors = {
        collection: [
          {
            _id: '1',
            birhday: '1995',
            country: 'Poland',
            gender: 'male',
            imdbId: '1',
            name: 'Brad Pitt',
            photoUrl: 'url'
          }
        ],
        total: 5
      };
      const action = actorsAction.actorsLoaded({ payload: { actors } });
      expect(actors.collection).toEqual(action.payload.actors.collection);
      expect(actors.total).toEqual(action.payload.actors.total);
    });
  });
});
