import { Actors, ActionPayload } from './../../shared/interfaces';
import { createAction, props } from '@ngrx/store';

export const loadActors = createAction('[Actors] Load actors');
export const actorsLoaded = createAction(
  '[Actors] Actor loaded',
  props<ActionPayload<{ actors: Actors }>>()
);
