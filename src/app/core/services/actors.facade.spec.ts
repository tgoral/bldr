import { Actor } from './../../shared/interfaces';
import { ActorsFacade } from './actors.facade';
import { Store } from '@ngrx/store';
import { AppState, selectActor } from '../reducers';
import { of } from 'rxjs';
import { loadActors } from '../actions/actors.actions';
describe('ActorsFacade', () => {
  let store: jasmine.SpyObj<Store<AppState>>;
  let actorsFacade: ActorsFacade;

  beforeEach(() => {
    store = jasmine.createSpyObj('store', ['dispatch', 'select']);
    actorsFacade = new ActorsFacade(store);
  });

  it('should find by imdbId', () => {
    const imdbId = '10';
    const actor = {
      imdbId
    } as Actor;
    const expected = of(actor);
    store.select.and.returnValue(expected);
    const result = actorsFacade.findByImdbId(imdbId);
    expect(result).toEqual(expected);
  });

  it('should dispatch loadActors', () => {
    actorsFacade.loadActors();
    expect(store.dispatch).toHaveBeenCalledWith(loadActors());
  });
});
