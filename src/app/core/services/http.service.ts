import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {
  ApiFindAllEndpoints,
  ApiFindByIdEndpoints,
  ApiPostEndpoints,
  ApiPostPayloadType,
  ApiQueryParamsType,
  ApiReturnType
} from './../../shared/types';

@Injectable({ providedIn: 'root' })
export class HttpService {
  readonly BASE_URL = environment.baseUrl;

  constructor(private httpClient: HttpClient) {}

  findAll<T extends keyof ApiFindAllEndpoints>(
    endpoint: T,
    queryParams: ApiQueryParamsType<T> = {}
  ): Observable<ApiReturnType<T>> {
    const params = Object.keys(queryParams).reduce(
      (par, key) => (par = { ...par, [key]: queryParams[key] + '' }),
      {}
    );
    return this.httpClient.get<ApiReturnType<T>>(this.BASE_URL + endpoint, { params });
  }

  findById<T extends keyof ApiFindByIdEndpoints>(
    endpoint: T,
    id: string
  ): Observable<ApiReturnType<T>> {
    return this.httpClient.get<ApiReturnType<T>>(this.BASE_URL + endpoint + id);
  }

  post<T extends keyof ApiPostEndpoints>(
    endpoint: T,
    payload: ApiPostPayloadType<T>
  ): Observable<ApiReturnType<T>> {
    return this.httpClient.post<ApiReturnType<T>>(this.BASE_URL + endpoint, payload);
  }
}
