import { SortDir } from './../../shared/enums';
import { MoviesSortBy } from 'src/app/shared/enums';
import { AuthToken, Pagination, MoviesPagination } from './../../shared/interfaces';
import { LocalStorageService } from './local-storage.service';

describe('LocalStorageService', () => {
  const localStorageService = new LocalStorageService();

  afterEach(() => localStorage.clear());

  it('should get token', () => {
    const token = 'xddd';
    localStorage.setItem('token', token);
    expect(localStorageService.getToken()).toEqual(token);
  });

  it('should save token', () => {
    const token: AuthToken = { token: ':ooooo' };
    localStorageService.setToken(token);
    expect(localStorage.getItem('token')).toEqual(token.token);
  });

  it('should remove token', () => {
    localStorage.setItem('token', 'xddd');
    localStorageService.removeToken();
    expect(localStorage.getItem('token')).toBeNull();
  });

  it('should set movies pagination without page', () => {
    const pagination: MoviesPagination = {
      sortBy: MoviesSortBy.Title,
      sortDir: SortDir.Asc,
      limit: 10,
      page: 2
    };
    localStorageService.setPagination(pagination);
    expect(localStorage.getItem('sortBy')).toEqual(pagination.sortBy + '');
    expect(localStorage.getItem('sortDir')).toEqual(pagination.sortDir + '');
    expect(localStorage.getItem('limit')).toEqual(pagination.limit + '');
    expect(localStorage.getItem('page')).toBeNull();
  });

  describe('getPagination', () => {
    it('should get without page', () => {
      const pagination: MoviesPagination = {
        sortBy: MoviesSortBy.Title,
        sortDir: SortDir.Asc,
        limit: 10,
        page: 2
      };
      Object.keys(pagination).forEach(key => localStorage.setItem(key, pagination[key]));
      const paginationFromStorage = localStorageService.getPagination();
      expect(paginationFromStorage.sortBy).toEqual(pagination.sortBy);
      expect(paginationFromStorage.sortDir).toEqual(pagination.sortDir);
      expect(paginationFromStorage.limit).toEqual(pagination.limit);
      expect(paginationFromStorage.page).toBeUndefined();
    });

    it('should not load invalid sortBy', () => {
      localStorage.setItem('sortBy', 'director');
      const paginationFromStorage = localStorageService.getPagination();
      expect(paginationFromStorage.sortBy).toBeUndefined();
    });

    it('should not load invalid sortDir', () => {
      localStorage.setItem('sortDir', 'asc');
      const paginationFromStorage = localStorageService.getPagination();
      expect(paginationFromStorage.sortDir).toBeUndefined();
    });

    it('should not load invalid limit', () => {
      localStorage.setItem('limit', '0');
      const paginationFromStorage = localStorageService.getPagination();
      expect(paginationFromStorage.limit).toBeUndefined();
    });
  });
});
