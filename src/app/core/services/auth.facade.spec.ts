import { logout } from './../actions/auth.actions';
import { LoginCredentials } from './../../shared/interfaces';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { Store } from '@ngrx/store';
import { AuthFacade } from './auth.facade';
import { Router } from '@angular/router';
import { AppState, selectIsAuthenticated } from '../reducers';
import { login } from '../actions/auth.actions';
import { Observable, of } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
describe('AuthFacade', () => {
  let authFacade: AuthFacade;
  let router: jasmine.SpyObj<Router>;
  let store: jasmine.SpyObj<Store<AppState>>;
  let localStorage: jasmine.SpyObj<LocalStorageService>;

  beforeEach(() => {
    router = jasmine.createSpyObj('router', ['navigate']);
    store = jasmine.createSpyObj('store', ['dispatch', 'select']);
    localStorage = jasmine.createSpyObj('localStorage', ['removeToken', 'getToken']);
    authFacade = new AuthFacade(router, store, localStorage);
  });

  it('should dispatch login action', () => {
    const credentials: LoginCredentials = {
      login: 'email@email',
      password: '1234'
    };
    const loginAction = login({ payload: credentials });
    authFacade.login(credentials);
    expect(store.dispatch).toHaveBeenCalledWith(loginAction);
  });

  it('should select is authenticated from store', () => {
    const expectedResult = of(true);
    store.select.withArgs(selectIsAuthenticated).and.returnValue(expectedResult);
    const result = authFacade.isAuthenticated();
    expect(result).toEqual(expectedResult);
  });

  it('should remove token, dispatch logout and navigate to login page', () => {
    authFacade.logout();
    expect(localStorage.removeToken).toHaveBeenCalled();
    expect(store.dispatch).toHaveBeenCalledWith(logout());
    expect(router.navigate).toHaveBeenCalledWith(['login']);
  });

  it('should return false when no token in storage', () => {
    localStorage.getToken.and.returnValue(null);
    const isValid = authFacade.isTokenValid();
    expect(isValid).toBeFalsy();
  });
});
