import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';
import { Movie } from 'src/app/shared/interfaces';
import { loadSelectedMovie, selectedMovieLoadedFromStore } from '../../movie/state/movie.actions';
import { State as MovieState } from '../../movie/state/movie.reducer';
import { selectSelectedMovie } from '../../movie/state/movie.selectors';
import {
  changeItemsPerPage,
  changePage,
  loadMovies,
  sortData
} from '../../movies/state/movies.actions';
import { State as MoviesState } from '../../movies/state/movies.reducer';
import {
  selectItemsPerPage,
  selectMoviesCollection,
  selectPageNumber,
  selectSorting,
  selectTotalNumber
} from '../../movies/state/movies.selectors';
import { MoviesSortBy } from '../../shared/enums';

@Injectable({ providedIn: 'root' })
export class MoviesFacade {
  limit$ = this.moviesStore.select(selectItemsPerPage);
  totalMovies$ = this.moviesStore.select(selectTotalNumber);
  pageNumber$ = this.moviesStore.select(selectPageNumber);
  movies$ = this.moviesStore.select(selectMoviesCollection);
  sort$ = this.moviesStore.select(selectSorting);
  selectedMovie$ = this.movieStore.select(selectSelectedMovie);

  constructor(
    private moviesStore: Store<MoviesState>,
    private movieStore: Store<MovieState>,
    private router: Router
  ) {}

  load() {
    this.moviesStore.dispatch(loadMovies());
  }

  loadSelectedMovie(id: string) {
    this.movieStore
      .select(selectSelectedMovie)
      .pipe(take(1))
      .subscribe(selectedMovie => {
        if (!selectedMovie || selectedMovie.imdbId !== id) {
          this.movieStore.dispatch(loadSelectedMovie({ payload: { id } }));
        }
      });
  }

  loadFromPage(page: number) {
    this.moviesStore.dispatch(changePage({ payload: { page } }));
    this.load();
  }

  loadWithLimit(limit: number) {
    this.moviesStore.dispatch(changeItemsPerPage({ payload: { limit } }));
    this.load();
  }

  loadWithSortedBy(sortBy: MoviesSortBy) {
    this.moviesStore.dispatch(sortData({ payload: { sortBy } }));
    this.load();
  }

  selectMovie(movie: Movie) {
    this.movieStore.dispatch(selectedMovieLoadedFromStore({ payload: { movie } }));
    this.router.navigate(['movies', movie.imdbId]);
  }
}
