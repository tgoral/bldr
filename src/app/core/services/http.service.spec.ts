import { MoviesSortBy, SortDir } from './../../shared/enums';
import { MoviesPagination, LoginCredentials } from './../../shared/interfaces';
import { HttpService } from './http.service';
import { HttpClient } from '@angular/common/http';

describe('HttpService', () => {
  let httpClient: jasmine.SpyObj<HttpClient>;
  let httpService: HttpService;

  beforeEach(() => {
    httpClient = jasmine.createSpyObj('httpClient', ['get', 'post']);
    httpService = new HttpService(httpClient);
  });

  describe('findAll', () => {
    it('should call httpClient without query params', () => {
      httpService.findAll('/movies');
      expect(httpClient.get).toHaveBeenCalledWith(httpService.BASE_URL + '/movies', { params: {} });
    });

    it('should call httpClient with query params', () => {
      const moviesPagination: MoviesPagination = {
        limit: 1,
        sortBy: MoviesSortBy.Title,
        sortDir: SortDir.Asc,
        page: 2
      };
      const moviesPaginationToString = {
        limit: '1',
        sortBy: 'title',
        sortDir: '-1',
        page: '2'
      };
      httpService.findAll('/movies', moviesPagination);
      expect(httpClient.get).toHaveBeenCalledWith(httpService.BASE_URL + '/movies', {
        params: moviesPaginationToString
      });
    });
  });

  it('should findById', () => {
    const id = '10';
    httpService.findById('/actors/', id);
    expect(httpClient.get).toHaveBeenCalledWith(httpService.BASE_URL + '/actors/' + id);
  });

  it('should post', () => {
    const payload: LoginCredentials = {
      login: 'email@email',
      password: '1234'
    };
    httpService.post('/auth/login', payload);
    expect(httpClient.post).toHaveBeenCalledWith(httpService.BASE_URL + '/auth/login', payload);
  });
});
