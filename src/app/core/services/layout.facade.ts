import { Injectable } from '@angular/core';
import { AppState, selectIsLoading } from '../reducers';
import { Store } from '@ngrx/store';

@Injectable({ providedIn: 'root' })
export class LayoutFacade {
  loading$ = this.store.select(selectIsLoading);

  constructor(private store: Store<AppState>) {}
}
