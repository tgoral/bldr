import { loadActors } from '../actions/actors.actions';
import { Injectable } from '@angular/core';
import { AppState, selectActor } from '../reducers';
import { Store } from '@ngrx/store';

@Injectable({ providedIn: 'root' })
export class ActorsFacade {
  constructor(private store: Store<AppState>) {}

  findByImdbId(imdbId: string) {
    return this.store.select(selectActor(imdbId));
  }

  loadActors() {
    this.store.dispatch(loadActors());
  }
}
