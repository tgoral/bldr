import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Store } from '@ngrx/store';
import { selectAuthErrorMessage } from '../reducers';
import { LoginCredentials } from './../../shared/interfaces';
import { login, logout } from './../actions/auth.actions';
import { AppState, selectIsAuthenticated } from './../reducers/index';
import { LocalStorageService } from './local-storage.service';

@Injectable({ providedIn: 'root' })
export class AuthFacade {
  errorMessage$ = this.store.select(selectAuthErrorMessage);
  private readonly jwtHelper = new JwtHelperService();

  constructor(
    private router: Router,
    private store: Store<AppState>,
    private localStorage: LocalStorageService
  ) {}

  login(credentials: LoginCredentials) {
    this.store.dispatch(login({ payload: credentials }));
  }

  isAuthenticated() {
    return this.store.select(selectIsAuthenticated);
  }

  logout() {
    this.localStorage.removeToken();
    this.store.dispatch(logout());
    this.router.navigate(['login']);
  }

  isTokenValid(): boolean {
    const token = this.localStorage.getToken();
    return token && !this.jwtHelper.isTokenExpired(token);
  }
}
