import { changePage, changeItemsPerPage, sortData } from './../../movies/state/movies.actions';
import { Movie } from './../../shared/interfaces';
import { MoviesFacade } from './movies.facade';
import { Store } from '@ngrx/store';
import { State as MoviesState } from 'src/app/movies/state/movies.reducer';
import { State as MovieState } from 'src/app/movie/state/movie.reducer';
import { Router } from '@angular/router';
import { loadMovies } from 'src/app/movies/state/movies.actions';
import { selectSelectedMovie } from 'src/app/movie/state/movie.selectors';
import { of, Observable, empty } from 'rxjs';
import { loadSelectedMovie, selectedMovieLoadedFromStore } from 'src/app/movie/state/movie.actions';
import { MoviesSortBy } from 'src/app/shared/enums';

describe('MoviesFacade', () => {
  let moviesStore: jasmine.SpyObj<Store<MoviesState>>;
  let movieStore: jasmine.SpyObj<Store<MovieState>>;
  let router: jasmine.SpyObj<Router>;
  let moviesFacade: MoviesFacade;

  beforeEach(() => {
    moviesStore = jasmine.createSpyObj('moviesStore', ['dispatch', 'select']);
    movieStore = jasmine.createSpyObj('movieStore', ['dispatch', 'select']);
    router = jasmine.createSpyObj('router', ['navigate']);
    moviesFacade = new MoviesFacade(moviesStore, movieStore, router);
  });

  it('should dispatch loadMovies', () => {
    moviesFacade.load();
    expect(moviesStore.dispatch).toHaveBeenCalledWith(loadMovies());
  });

  describe('loadSelectedMovie', () => {
    it('should dispatch loadSelectedMovie action when selectedMovie is null', () => {
      movieStore.select.withArgs(selectSelectedMovie).and.returnValue(of(null));
      const id = '10';
      moviesFacade.loadSelectedMovie(id);
      expect(movieStore.dispatch).toHaveBeenCalledWith(loadSelectedMovie({ payload: { id } }));
    });

    it('should dispatch loadSelectedMovie action when selectedMovie has different imdbId', () => {
      const currentSelectedMovie = {
        imdbId: '20'
      } as Movie;
      movieStore.select.withArgs(selectSelectedMovie).and.returnValue(of(currentSelectedMovie));
      const id = '10';
      moviesFacade.loadSelectedMovie(id);
      expect(movieStore.dispatch).toHaveBeenCalledWith(loadSelectedMovie({ payload: { id } }));
    });

    it('should not dispatch loadSelectedMovie action when selectedMovie is already loaded', () => {
      const currentSelectedMovie = {
        imdbId: '10'
      } as Movie;
      movieStore.select.withArgs(selectSelectedMovie).and.returnValue(of(currentSelectedMovie));
      const id = '10';
      moviesFacade.loadSelectedMovie(id);
      expect(movieStore.dispatch).not.toHaveBeenCalledWith();
    });
  });

  it('should dispatch changePage and loadMovies', () => {
    const page = { page: 2 };
    moviesFacade.loadFromPage(page.page);
    expect(moviesStore.dispatch).toHaveBeenCalledWith(loadMovies());
    expect(moviesStore.dispatch).toHaveBeenCalledWith(changePage({ payload: page }));
  });

  it('should dispatch changeItemsPerPage and loadMovies', () => {
    const limit = { limit: 2 };
    moviesFacade.loadWithLimit(limit.limit);
    expect(moviesStore.dispatch).toHaveBeenCalledWith(loadMovies());
    expect(moviesStore.dispatch).toHaveBeenCalledWith(changeItemsPerPage({ payload: limit }));
  });

  it('should dispatch sortData and loadMovies', () => {
    const sortBy = { sortBy: MoviesSortBy.Title };
    moviesFacade.loadWithSortedBy(sortBy.sortBy);
    expect(moviesStore.dispatch).toHaveBeenCalledWith(loadMovies());
    expect(moviesStore.dispatch).toHaveBeenCalledWith(sortData({ payload: sortBy }));
  });

  it('should dispatch selectedMovieLoadedFromStore and navigate to it', () => {
    const movie: Movie = {
      _id: '2',
      actors: [
        {
          imdbId: '30',
          name: 'Brad Pitt'
        }
      ],
      director: 'Spielberg',
      imdbId: '30',
      metascore: 60,
      posterUrl: 'url',
      title: 'Szeregowiec Ryan',
      year: 2010
    };
    moviesFacade.selectMovie(movie);
    expect(movieStore.dispatch).toHaveBeenCalledWith(
      selectedMovieLoadedFromStore({ payload: { movie } })
    );
    expect(router.navigate).toHaveBeenCalledWith(['movies', movie.imdbId]);
  });
});
