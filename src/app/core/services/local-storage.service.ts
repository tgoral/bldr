import { Injectable } from '@angular/core';
import { isFromEnum, MoviesSortBy, SortDir } from './../../shared/enums';
import { AuthToken } from './../../shared/interfaces';
import { MoviesPagination } from './../../shared/interfaces';

@Injectable({ providedIn: 'root' })
export class LocalStorageService {
  getToken(): string {
    return this.getItem('token');
  }

  setToken(token: AuthToken) {
    this.setItems(token);
  }

  removeToken() {
    localStorage.removeItem('token');
  }

  setPagination(pagination: MoviesPagination) {
    const { page, ...toCache } = pagination;
    this.setItems(toCache);
  }

  getPagination(): MoviesPagination {
    const pagination: MoviesPagination = {};
    const cachedSortBy = this.getItem('sortBy');
    const cachedSortDir = Number(this.getItem('sortDir'));
    const cachedLimit = Number(this.getItem('limit'));
    if (isFromEnum(MoviesSortBy, cachedSortBy)) {
      pagination.sortBy = cachedSortBy as MoviesSortBy;
    }
    if (isFromEnum(SortDir, cachedSortDir)) {
      pagination.sortDir = cachedSortDir as SortDir;
    }
    if (cachedLimit != null && !isNaN(cachedLimit) && cachedLimit > 0) {
      pagination.limit = cachedLimit;
    }
    return pagination;
  }

  private setItems(obj: object) {
    if (obj) {
      Object.keys(obj).forEach(key => localStorage.setItem(key, obj[key]));
    }
  }

  private getItem(key: keyof MoviesPagination | keyof AuthToken): string {
    return localStorage.getItem(key);
  }
}
