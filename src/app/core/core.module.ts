import { IsAuthenticatedGuard } from './guards/is-authenitcated.guard';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import { SharedModule } from '../shared/shared.module';
import { ActorsEffects } from './effects/actors.effects';
import { AuthEffects } from './effects/auth.effects';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { reducers } from './reducers';
import { metaReducers } from './reducers/index';

@NgModule({
  declarations: [HeaderComponent, FooterComponent],
  imports: [
    SharedModule,
    HttpClientModule,
    RouterModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    environment.production ? [] : StoreDevtoolsModule.instrument({ maxAge: 25 }),
    EffectsModule.forRoot([ActorsEffects, AuthEffects]),
    BrowserAnimationsModule
  ],
  exports: [HeaderComponent, FooterComponent],
  providers: [
    IsAuthenticatedGuard,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ]
})
export class CoreModule {}
