import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MovieRoutingModule } from './movie-routing.module';
import { MovieComponent } from './movie.component';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { StoreModule } from '@ngrx/store';
import { movieReducer } from './state/movie.reducer';
import { EffectsModule } from '@ngrx/effects';
import { MovieEffects } from './state/movie.effects';

@NgModule({
  declarations: [MovieComponent, MovieDetailsComponent],
  imports: [
    CommonModule,
    MovieRoutingModule,
    StoreModule.forFeature('movie', movieReducer),
    EffectsModule.forFeature([MovieEffects])
  ]
})
export class MovieModule {}
