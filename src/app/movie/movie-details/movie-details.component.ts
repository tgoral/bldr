import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Movie } from 'src/app/shared/interfaces';
import { ActorsFacade } from '../../core/services/actors.facade';
import { Actor } from './../../shared/interfaces';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MovieDetailsComponent {
  @Input() movie: Movie;

  readonly imdbActorLink = 'https://www.imdb.com/name/';
  readonly imdbMovieLink = 'https://www.imdb.com/title/';

  constructor(private actorsFacade: ActorsFacade) {}

  findPhotoOfActor(givenActor: Actor): Observable<string> {
    return this.actorsFacade.findByImdbId(givenActor.imdbId).pipe(
      filter(actor => !!actor),
      map(actor => actor.photoUrl)
    );
  }
}
