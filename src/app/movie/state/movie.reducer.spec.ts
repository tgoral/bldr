import { ErrorMessage } from 'src/app/shared/interfaces';
import { Movie } from './../../shared/interfaces';
import * as movieAction from './movie.actions';
import { movieReducer, initState, MovieState } from './movie.reducer';

describe('Movie reducer', () => {
  it('should return the default state', () => {
    const state = movieReducer(undefined, {} as any);
    expect(state).toBe(initState);
  });

  it('should set selected movie', () => {
    const testInitState: MovieState = {
      selectedMovie: null,
      errorMessage: 'error'
    };
    const movie: Movie = {
      _id: '1',
      actors: [
        {
          imdbId: '20',
          name: 'Brad Pitt'
        }
      ],
      director: 'Spielberg',
      imdbId: '10',
      metascore: 50,
      posterUrl: 'url',
      title: 'Koń',
      year: 2010
    };
    const state = movieReducer(
      testInitState,
      movieAction.loadSelectedMovieSuccess({ payload: { movie } })
    );
    expect(state.selectedMovie).toBe(movie);
    expect(state.errorMessage).toBeNull();
  });

  it('should set error message', () => {
    const testInitState: MovieState = {
      selectedMovie: null,
      errorMessage: null
    };
    const error: ErrorMessage = {
      errorMessage: 'xd'
    };
    const state = movieReducer(
      testInitState,
      movieAction.loadSelectedMovieError({ payload: error })
    );
    expect(state.errorMessage).toBe(error.errorMessage);
  });
});
