import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, switchMap, catchError } from 'rxjs/operators';
import { HttpService } from './../../core/services/http.service';
import {
  loadSelectedMovie,
  loadSelectedMovieError,
  loadSelectedMovieSuccess
} from './movie.actions';
import { of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class MovieEffects {
  @Effect() loadSelectedMovie$ = this.actions$.pipe(
    ofType(loadSelectedMovie),
    switchMap(action =>
      this.httpService.findById('/movies/', action.payload.id).pipe(
        map(movie => loadSelectedMovieSuccess({ payload: { movie } })),
        catchError((error: HttpErrorResponse) =>
          of(loadSelectedMovieError({ payload: { errorMessage: error.message } }))
        )
      )
    )
  );

  constructor(private actions$: Actions, private httpService: HttpService) {}
}
