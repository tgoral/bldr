import { AppState } from './../../core/reducers/index';
import { Movie, ErrorMessage } from 'src/app/shared/interfaces';
import { createReducer, on, Action } from '@ngrx/store';
import {
  loadSelectedMovieError,
  selectedMovieLoadedFromStore,
  loadSelectedMovieSuccess
} from './movie.actions';

export interface State extends AppState {
  movie: MovieState;
}

export interface MovieState extends ErrorMessage {
  selectedMovie: Movie | null;
}

export const initState: MovieState = { selectedMovie: null, errorMessage: null };

const reducer = createReducer(
  initState,
  on(loadSelectedMovieSuccess, selectedMovieLoadedFromStore, (state, action) => ({
    ...state,
    selectedMovie: action.payload.movie,
    errorMessage: null
  })),
  on(loadSelectedMovieError, (state, action) => ({
    ...state,
    errorMessage: action.payload.errorMessage
  }))
);

export function movieReducer(state: MovieState, action: Action) {
  return reducer(state, action);
}
