import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State, MovieState } from './movie.reducer';

const selectSelectedMovieState = createFeatureSelector<State, MovieState>('movie');
export const selectSelectedMovie = createSelector(
  selectSelectedMovieState,
  movie => movie.selectedMovie
);

export const selectMovieErrorMessage = createSelector(
  selectSelectedMovieState,
  movie => movie.errorMessage
);
