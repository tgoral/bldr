import { Movie, ErrorMessage } from './../../shared/interfaces';
import { MovieEffects } from './movie.effects';
import { HttpService } from './../../core/services/http.service';
import { of, ReplaySubject, throwError } from 'rxjs';
import { loadSelectedMovie } from './movie.actions';

describe('MovieEffects', () => {
  let httpService: jasmine.SpyObj<HttpService>;

  beforeEach(() => {
    httpService = jasmine.createSpyObj('httpService', ['findById']);
  });

  describe('loadSelectedMovie$', () => {
    it('should sucess', () => {
      const actions: ReplaySubject<any> = new ReplaySubject(1);
      const id = '10';
      actions.next(loadSelectedMovie({ payload: { id } }));
      const movieEffects = new MovieEffects(actions, httpService);
      const movie: Movie = {
        _id: '1',
        actors: [
          {
            imdbId: '20',
            name: 'Brad Pitt'
          }
        ],
        director: 'Spielberg',
        imdbId: '10',
        metascore: 50,
        posterUrl: 'url',
        title: 'Koń',
        year: 2010
      };
      httpService.findById.withArgs('/movies/', id).and.returnValue(of(movie));
      movieEffects.loadSelectedMovie$.subscribe(result => {
        expect((result.payload as { movie: Movie }).movie).toBe(movie);
      });
    });

    it('should failure', () => {
      const actions: ReplaySubject<any> = new ReplaySubject(1);
      const id = '10';
      actions.next(loadSelectedMovie({ payload: { id } }));
      const movieEffects = new MovieEffects(actions, httpService);
      const error = {
        message: 'test err'
      };
      httpService.findById.withArgs('/movies/', id).and.returnValue(throwError(error));
      movieEffects.loadSelectedMovie$.subscribe(result => {
        expect((result.payload as ErrorMessage).errorMessage).toBe(error.message);
      });
    });
  });
});
