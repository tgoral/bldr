import { ActionPayload, ErrorMessage } from './../../shared/interfaces';
import { Movie } from 'src/app/shared/interfaces';
import { props, createAction } from '@ngrx/store';

export const loadSelectedMovie = createAction(
  '[Movie] Load selected movie',
  props<ActionPayload<{ id: string }>>()
);
export const loadSelectedMovieSuccess = createAction(
  '[Movie] Load selected movie success',
  props<ActionPayload<{ movie: Movie }>>()
);
export const loadSelectedMovieError = createAction(
  '[Movie] Load selected movie error',
  props<ActionPayload<ErrorMessage>>()
);
export const selectedMovieLoadedFromStore = createAction(
  '[Movie] Selected movie loaded from store',
  props<ActionPayload<{ movie: Movie }>>()
);
