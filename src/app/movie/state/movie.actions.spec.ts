import { Movie, ErrorMessage } from 'src/app/shared/interfaces';
import * as movieAction from './movie.actions';

const exampleMovie: Movie = {
  _id: '1',
  actors: [
    {
      imdbId: '20',
      name: 'Brad Pitt'
    }
  ],
  director: 'Spielberg',
  imdbId: '10',
  metascore: 50,
  posterUrl: 'url',
  title: 'Koń',
  year: 2010
};

describe('Movie actions', () => {
  describe('loadSelectedMovie', () => {
    it('should create an action', () => {
      const movieId = { id: '1' };
      const action = movieAction.loadSelectedMovie({ payload: movieId });
      expect(action.type).toEqual(movieAction.loadSelectedMovie.type);
      expect(action.payload).toEqual(movieId);
    });
  });

  describe('loadSelectedMovieSuccess', () => {
    it('should create an action', () => {
      const movie: Movie = exampleMovie;
      const action = movieAction.loadSelectedMovieSuccess({ payload: { movie } });
      expect(action.type).toEqual(movieAction.loadSelectedMovieSuccess.type);
      expect(action.payload.movie).toEqual(movie);
    });
  });

  describe('loadSelectedMovieError', () => {
    it('should create an action', () => {
      const error: ErrorMessage = { errorMessage: 'test err' };
      const action = movieAction.loadSelectedMovieError({ payload: error });
      expect(action.type).toEqual(movieAction.loadSelectedMovieError.type);
      expect(action.payload).toEqual(error);
    });
  });

  describe('selectedMovieLoadedFromStore', () => {
    it('should create an action', () => {
      const movie: Movie = exampleMovie;
      const action = movieAction.selectedMovieLoadedFromStore({ payload: { movie } });
      expect(action.type).toEqual(movieAction.selectedMovieLoadedFromStore.type);
      expect(action.payload.movie).toEqual(movie);
    });
  });
});
