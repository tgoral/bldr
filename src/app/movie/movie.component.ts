import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { MoviesFacade } from '../core/services/movies.facade';
import { Movie } from '../shared/interfaces';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MovieComponent implements OnInit, OnDestroy {
  movie$ = this.moviesFacade.selectedMovie$;
  ngUnsubsribe$ = new Subject();

  constructor(private moviesFacade: MoviesFacade, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.params.pipe(takeUntil(this.ngUnsubsribe$)).subscribe(({ id }) => {
      this.moviesFacade.loadSelectedMovie(id);
    });
  }

  ngOnDestroy() {
    this.ngUnsubsribe$.next();
    this.ngUnsubsribe$.complete();
  }
}
